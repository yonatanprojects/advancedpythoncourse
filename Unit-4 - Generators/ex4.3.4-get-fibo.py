def get_fibo():
    num1 = 0
    num2 = 1
    yield num1
    yield num2
    while True:
        temp = num2
        num2 += num1
        num1 = temp
        yield num2


def main():
    fibo_gen = get_fibo()
    print(next(fibo_gen))
    print(next(fibo_gen))
    print(next(fibo_gen))
    print(next(fibo_gen))


if __name__ == '__main__':
    main()
