"""
Concluding Exercise for Unit 4:
Create generator functions that generate numbers for all seconds, minutes and hours.
Also, create a generator function that generates the time using the 3 previous generators.
In addition create generator functions that generate numbers for every year from now on,
every month and for the days in each month considering if the year is a leap year.
Then, create a generator that generates all dates and times from said generator functions.
Print every 1000000th date and time.
"""


def gen_secs():
    sec = 0
    while sec < 60:
        yield sec
        sec += 1


def gen_minutes():
    minute = 0
    while minute < 60:
        yield minute
        minute += 1


def gen_hours():
    hour = 0
    while hour < 24:
        yield hour
        hour += 1


def gen_time():
    for h in gen_hours():
        for m in gen_minutes():
            for s in gen_secs():
                yield "%02d:%02d:%02d" % (h, m, s)


def gen_years(start=2023):
    while True:
        yield start
        start += 1


def gen_months():
    month = 0
    while month < 12:
        month += 1
        yield month


def gen_days(month, leap_year):
    days_in_months = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]
    day = 0
    num_of_days = days_in_months[month - 1]
    if leap_year and month == 2:
        num_of_days += 1
    while day < num_of_days:
        day += 1
        yield day


def is_leap_year(year):
    if year % 4 == 0:
        if year % 100 == 0:
            if year % 400 == 0:
                return True
            else:
                return False
        return True
    return False


def gen_date():
    for y in gen_years():
        for m in gen_months():
            for d in gen_days(m, is_leap_year(y)):
                for t in gen_time():
                    yield "%02d/%02d/%04d %s" % (d, m, y, t)


def main():
    iteration = 0
    for date in gen_date():
        if iteration % 1000000 == 0:
            print(date)
        iteration += 1


if __name__ == '__main__':
    main()
