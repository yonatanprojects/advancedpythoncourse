def translate(sentence):
    words = {'esta': 'is', 'la': 'the', 'en': 'in', 'gato': 'cat', 'casa': 'house', 'el': 'the'}
    sentence = sentence.split(' ')
    gen = [words[word] for word in sentence]
    for word in gen:
        print(word, end=' ')


def main():
    translate("el gato esta en la casa")


if __name__ == '__main__':
    main()
