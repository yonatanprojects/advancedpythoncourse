def read_file(file_name):
    print("__CONTENT_START__")
    try:
        file = open(file_name)
    except IOError:
        print("__NO_SUCH_FILE__")
    else:
        lines = file.read()
        print(lines)
    finally:
        print("__CONTENT_END__")


def main():
    read_file("some_file.txt")
    print()
    read_file("names.txt")


if __name__ == '__main__':
    main()
