"""
Concluding Exercise for Unit 3:
Create custom Exception Classes for given passwords and usernames.
E.g. Username/Password being too long, too short, missing a character, etc.
Checking for every username and password if they are valid.
If not, raising the appropriate exception and printing an appropriate message for it.
"""

import string


class UsernameContainsIllegalCharacter(Exception):
    def __init__(self, username, char):
        self._username = username
        self._char = char

    def __str__(self):
        return f"The username contains an illegal character \"{self._char}\" at index" \
               f" {self._username.index(self._char)}"


class UsernameTooShort(Exception):
    def __str__(self):
        return "The username is too short"


class UsernameTooLong(Exception):
    def __str__(self):
        return "The username is too long"


class PasswordMissingCharacter(Exception):
    def __str__(self):
        return "The password is missing a character"


class MissingUppercase(PasswordMissingCharacter):
    def __str__(self):
        return super().__str__() + " (Uppercase)"


class MissingLowercase(PasswordMissingCharacter):
    def __str__(self):
        return super().__str__() + " (Lowercase)"


class MissingDigit(PasswordMissingCharacter):
    def __str__(self):
        return super().__str__() + " (Digit)"


class MissingSpecial(PasswordMissingCharacter):
    def __str__(self):
        return super().__str__() + " (Special)"


class PasswordTooShort(Exception):
    def __str__(self):
        return "The password is too short"


class PasswordTooLong(Exception):
    def __str__(self):
        return "The password is too long"


def check_input(username, password):
    try:
        if len(username) < 3:
            raise UsernameTooShort
        elif len(username) > 16:
            raise UsernameTooLong
        for char in username:
            if not char.isalpha() and not char.isnumeric() and char != "_":
                raise UsernameContainsIllegalCharacter(username, char)
        if len(password) < 8:
            raise PasswordTooShort
        elif len(password) > 40:
            raise PasswordTooLong
        flags = [False, False, False, False]
        for char in password:
            if char.isalpha():
                if char.isupper():
                    flags[0] = True
                elif char.islower():
                    flags[1] = True
            elif char.isnumeric():
                flags[2] = True
            elif char in string.punctuation:
                flags[3] = True
        if not flags[0]:
            raise MissingUppercase
        elif not flags[1]:
            raise MissingLowercase
        elif not flags[2]:
            raise MissingDigit
        elif not flags[3]:
            raise MissingSpecial
        print("OK")
    except UsernameTooShort as e:
        print(e.__str__())
    except UsernameTooLong as e:
        print(e.__str__())
    except UsernameContainsIllegalCharacter as e:
        print(e.__str__())
    except PasswordTooShort as e:
        print(e.__str__())
    except PasswordTooLong as e:
        print(e.__str__())
    except PasswordMissingCharacter as e:
        print(e.__str__())


def main():
    check_input("1", "2")
    check_input("0123456789ABCDEFG", "2")
    check_input("A_a1.", "12345678")
    check_input("A_1", "2")
    check_input("A_1", "ThisIsAQuiteLongPasswordAndHonestlyUnnecessary")
    check_input("A_1", "abcdefghijklmnop")
    check_input("A_1", "ABCDEFGHIJLKMNOP")
    check_input("A_1", "ABCDEFGhijklmnop")
    check_input("A_1", "4BCD3F6h1jk1mn0p")
    check_input("A_1", "4BCD3F6.1jk1mn0p")


if __name__ == '__main__':
    main()
