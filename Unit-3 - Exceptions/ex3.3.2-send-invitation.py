class UnderAge(Exception):
    def __init__(self, name, age):
        self.name = name
        self.age = age

    def __str__(self):
        years_until_18 = 18 - int(self.age)
        return f"{self.name} cannot be invited to the party since they're underage. " \
               f"Their age is {self.age} and there are {years_until_18} years until they turn 18."


def send_invitation(name, age):
    if int(age) < 18:
        raise UnderAge(name, age)
    else:
        print("You should send an invite to " + name)


def main():
    try:
        send_invitation("Simon", 20)
        send_invitation("Harry", 17)
    except UnderAge:
        print("Error while inviting an underage person.")


if __name__ == '__main__':
    main()
