def stop_iteration():
    iterator = iter([])
    next(iterator)  # Raises StopIteration error


def zero_division():
    return 5 / 0


def assertion_error():
    assert False, "AssertionError raised"


"""
def import_error():
    import non_existing_module
"""


def key_error():
    my_dict = {"key1": "value1", "key2": "value2"}
    value = my_dict["non_existing_key"]


def syntax_error():
    eval('random string')


"""
def indentation_error():
    if True:
    print("oops")
"""


def type_error():
    result = "Hello" + 123


def main():
    pass
    # stop_iteration()
    # zero_division()
    # assertion_error()
    # import_error()
    # key_error()
    # syntax_error()
    # indentation_error()
    # type_error()


if __name__ == '__main__':
    main()
