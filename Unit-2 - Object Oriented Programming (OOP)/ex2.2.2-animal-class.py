class Animal:
    def __init__(self, name, age):
        self.name = name
        self.age = age

    def birthday(self):
        self.age += 1

    def get_age(self):
        return self.age


def main():
    octopus = Animal("Octavio", 2)
    bunny = Animal("Dr. Colosso", 5)
    bunny.birthday()
    print(octopus.get_age())
    print(bunny.get_age())


if __name__ == "__main__":
    main()
