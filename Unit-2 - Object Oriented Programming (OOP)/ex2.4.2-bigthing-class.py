class BigThing:
    def __init__(self, var):
        self._var = var

    def size(self):
        if isinstance(self._var, int):
            return self._var
        elif isinstance(self._var, str) or isinstance(self._var, list) or isinstance(self._var, dict):
            return len(self._var)


class BigCat(BigThing):
    def __init__(self, name, weight):
        super().__init__(name)
        self._weight = weight

    def size(self):
        if self._weight > 20:
            return "Very Fat"
        if self._weight > 15:
            return "Fat"
        return "OK"


def main():
    my_thing = BigThing("balloon")
    print(my_thing.size())
    cutie = BigCat("mitzy", 22)
    print(cutie.size())


if __name__ == '__main__':
    main()
