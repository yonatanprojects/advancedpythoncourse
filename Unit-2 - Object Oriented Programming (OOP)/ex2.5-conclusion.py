"""
Concluding Exercise for Unit 2:
Create a super class called Animal and sub-classes called Dog, Cat, Skunk, Unicorn and Dragon.
Give each of them their characteristics (a unique saying and an activity).
For every animal in the zoo - Print it's type, feed it, let the animal "talk" and call their own activity.
"""


class Animal:
    zoo_name = "Hayaton"

    def __init__(self, name, hunger=0):
        self._name = name
        self._hunger = hunger

    def get_name(self):
        return self._name

    def is_hungry(self):
        if self._hunger > 0:
            return True
        return False

    def feed(self):
        if self._hunger > 0:
            self._hunger -= 1

    @staticmethod
    def talk(saying):
        print(saying)


class Dog(Animal):
    def talk(self):
        super().talk("woof woof")

    @staticmethod
    def fetch_stick():
        print("There you go, sir!")


class Cat(Animal):
    def talk(self):
        super().talk("meow")

    @staticmethod
    def chase_laser():
        print("Meeeeow")


class Skunk(Animal):
    def __init__(self, name, hunger=0, stink_count=6):
        super().__init__(name, hunger)
        self._stink_count = stink_count

    def talk(self):
        super().talk("tsssss")

    @staticmethod
    def stink():
        print("Dear lord!")


class Unicorn(Animal):
    def talk(self):
        super().talk("Good day, darling")

    @staticmethod
    def sing():
        print("I’m not your toy...")


class Dragon(Animal):
    def __init__(self, name, hunger=0, color="Green"):
        super().__init__(name, hunger)
        self._color = color

    def talk(self):
        super().talk("Raaaawr")

    @staticmethod
    def breath_fire():
        print("$@#$#@$")


def main():
    zoo_lst = [Dog("Brownie", 10), Cat("Zelda", 3), Skunk("Stinky"), Unicorn("Keith", 7), Dragon("Lizzy", 1450),
               Dog("Doggo", 80), Cat("Kitty", 80), Skunk("Stinky Jr.", 80), Unicorn("Clair", 80), Dragon("McFly", 80)]
    for animal in zoo_lst:
        if animal.is_hungry():
            print(type(animal).__name__, animal.get_name())
        animal.talk()
        while animal.is_hungry():
            animal.feed()
        if isinstance(animal, Dog):
            animal.fetch_stick()
        elif isinstance(animal, Cat):
            animal.chase_laser()
        elif isinstance(animal, Skunk):
            animal.stink()
        elif isinstance(animal, Unicorn):
            animal.sing()
        elif isinstance(animal, Dragon):
            animal.breath_fire()
    print(Animal.zoo_name)


if __name__ == '__main__':
    main()
