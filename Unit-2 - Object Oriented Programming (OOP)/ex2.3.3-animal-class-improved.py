class Animal:
    count_animals = 0

    def __init__(self, age, name="Octavio"):
        self._age = age
        self._name = name
        Animal.count_animals += 1

    def birthday(self):
        self._age += 1

    def get_age(self):
        return self._age

    def set_name(self, name):
        self._name = name

    def get_name(self):
        return self._name


def main():
    octopus = Animal(2)
    bunny = Animal(5, "Dr. Colosso")
    bunny.birthday()
    print("Name: " + octopus.get_name() + " | Age:", octopus.get_age())
    print("Name: " + bunny.get_name() + " | Age:", bunny.get_age())
    octopus.set_name("Dr. Octavius")
    print("Name: " + octopus.get_name() + " | Age:", octopus.get_age())
    print("Number of animals created so far:", Animal.count_animals)


if __name__ == "__main__":
    main()
