class GreetingCard:
    def __init__(self, sender="Eyal Ch", recipient="Dana Ev"):
        self._sender = sender
        self._recipient = recipient

    def greeting_msg(self):
        print(f"The sender is {self._sender} and the recipient is {self._recipient}.")
