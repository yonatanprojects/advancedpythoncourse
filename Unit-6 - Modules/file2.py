from file1 import GreetingCard


class BirthdayCard(GreetingCard):
    def __init__(self, sender_age=0, sender="Eyal Ch", recipient="Dana Ev"):
        super().__init__(sender, recipient)
        self._sender_age = sender_age

    def greeting_msg(self):
        super().greeting_msg()
        print(f"The age of the sender is {self._sender_age}.")
