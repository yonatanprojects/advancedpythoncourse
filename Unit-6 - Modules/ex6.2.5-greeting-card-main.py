from file1 import GreetingCard
from file2 import BirthdayCard


def main():
    gc = GreetingCard()
    bc = BirthdayCard(18, "Jonathan Wolf", "Peter Parker")
    gc.greeting_msg()
    bc.greeting_msg()


if __name__ == '__main__':
    main()
