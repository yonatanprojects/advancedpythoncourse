import tkinter as tk
import os


def reveal_image():
    label.config(text="Obviously, the best group on YouTube is...")  # Changing label text
    image_label.pack()  # Showing the image


root = tk.Tk()  # Creating the main window
label = tk.Label(root, text="What's the best group on YouTube?")
label.pack()  # Creating the label
button = tk.Button(root, text="Click to find out!", command=reveal_image)
button.pack()  # Creating the button
image_path = os.path.abspath("sidemen.png")  # Getting the absolute file path of the image
image = tk.PhotoImage(file=image_path)  # Loading the image
image_label = tk.Label(root, image=image)  # Creating the image label but keep it hidden
image_label.pack_forget()  # Hiding the image initially
root.mainloop()  # Running the main event loop
