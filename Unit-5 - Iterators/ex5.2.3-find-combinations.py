import itertools


def find_combinations():
    bills = [20, 20, 20, 10, 10, 10, 10, 10, 5, 5, 1, 1, 1, 1, 1]
    combinations = []
    for i in range(len(bills)):
        for c in itertools.combinations(bills, i):
            if sum(c) == 100 and c not in combinations:
                combinations.append(c)
    return combinations


def main():
    all_combinations = find_combinations()
    run = iter(all_combinations)
    i = 0
    while True:
        i += 1
        try:
            print(f"{i}.", next(run))
        except StopIteration:
            break


if __name__ == '__main__':
    main()
