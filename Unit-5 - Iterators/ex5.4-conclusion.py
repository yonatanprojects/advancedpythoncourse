"""
Concluding Exercise for Unit 5:
Create a custom iterator class and a generator function that generate valid IDs.
Allow the user to pick either "it" for iterator or "gen" for generator.
Use the given choice to generate and print the next 10 IDs after the given ID by the user.
"""


class IDIterator:
    def __init__(self, _id):
        self._id = _id

    def __iter__(self):
        return self

    def __next__(self):
        self._id += 1
        while not check_id_valid(self._id):
            self._id += 1
            if self._id >= 999999999:
                raise StopIteration
        return self._id


def id_generator(id_number):
    id_number += 1
    while id_number <= 999999999:
        if check_id_valid(id_number):
            yield id_number
        id_number += 1


def check_id_valid(id_number):
    lst = list(str(id_number))
    index = 0
    sum_all = 0
    for num in lst:
        index += 1
        result = int(num) * 2 if index % 2 == 0 else int(num)
        sum_all += int(result / 10) + (result % 10) if result > 9 else result
    if sum_all % 10 != 0:
        return False
    return True


def main():
    _id = input("Enter an ID: ")
    choice = input("Generator or Iterator? (gen/it)? ")
    if choice == "it" or choice == "gen":
        if choice == "it":
            iterable_obj = iter(IDIterator(int(_id)))
        else:
            iterable_obj = id_generator(int(_id))
        i = 0
        for _id in iterable_obj:
            i += 1
            print(_id)
            if i == 10:
                break
    else:
        print("Please enter either \"gen\" or \"it\"")


if __name__ == '__main__':
    main()
