class MusicNotes:
    def __init__(self):
        self._frequencies = [55, 61.74, 65.41, 73.42, 82.41, 87.31, 98]
        self._count_mul = 1
        self._index = 0

    def __iter__(self):
        return self

    def get_freq(self):
        save = self._frequencies[self._index]
        self._frequencies[self._index] *= 2
        self._index += 1
        return save

    def __next__(self):
        if self._index == len(self._frequencies):
            self._count_mul += 1
            self._index = 0
        if self._count_mul > 5:
            raise StopIteration
        freq = self.get_freq()
        return freq


def main():
    notes_iter = iter(MusicNotes())
    for freq in notes_iter:
        print(freq)


if __name__ == '__main__':
    main()
