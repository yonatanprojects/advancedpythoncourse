import winsound

freqs = {"la": 220, "si": 247, "do": 261, "re": 293, "mi": 329, "fa": 349, "sol": 392, }
notes = "sol,250-mi,250-mi,500-fa,250-re,250-re,500-do,250-re,250-mi,250-fa,250-sol,250-sol,250-sol,500"
notes_list = notes.split("-")
print(type(notes_list))
print("__iter__" in dir(notes_list))

run = iter(notes_list)
while True:
    try:
        note = next(run)
        note_info = note.split(",")
        print(note_info)
        winsound.Beep(freqs[note_info[0]], int(note_info[1]))
    except StopIteration:
        break
