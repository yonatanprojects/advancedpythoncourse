# advancedPythonCourse



## What is this?

This project consists solutions for all of the exercises featured on the "next.py" python course from the CampusIL website.

I've been asked to complete this course and upload my solutions to GitLab, so here it is.

## What does this course consists?

The course consists 6 Units, each referring to a different aspect in the slightly advanced Python world.
These are the 6 units of the course:

- [1] One-Liners
- [2] Object Oriented Programming (OOP)
- [3] Exceptions
- [4] Generators
- [5] Iterators
- [6] Modules

## What does each unit consist?

As mentioned earlier, each unit is representing a different aspect of Python.
Each unit consists of several exercises on the subject of the unit.
In addition, at the end of each unit there is a concluding exercise related to the topic of the unit.
 - Each exercise is named in a way that indicates it's purpose.

## Thanks for reading this!

Hopefully you will enjoy my project.
 - Yonatan Wolf
 
