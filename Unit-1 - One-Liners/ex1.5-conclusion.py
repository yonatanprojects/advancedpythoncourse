"""
Concluding Exercise for Unit 1:
Use one-liners to find the longest name in a file, the sum of all lengths of names,
the shortest names, to create a file of the name-lengths and to print all names of a certain length.
"""


def print_longest_name():
    lines = open("names.txt", "r").read().split("\n")
    return max(lines)


def print_sum_length():
    lines = open("names.txt", "r").read()
    return sum(len(name.strip()) for name in lines)


def print_shortest_names():
    lines = open("names.txt", "r").read().split("\n")
    lengths = [len(word) for word in lines]
    shortest = [name for name in lines if len(name) == min(lengths)]
    return "\n".join(shortest)


def create_length_file():
    lines = open("names.txt", "r").read().split("\n")
    lengths = [str(len(word)) for word in lines]
    open("name_length.txt", "w").write("\n".join(lengths))


def print_names_of_length():
    num = input("Enter name length: ")
    lines = open("names.txt", "r").read().split("\n")
    return "\n".join([name for name in lines if len(name) == int(num)])


def main():
    print(print_longest_name())
    print(print_sum_length())
    print(print_shortest_names())
    create_length_file()
    print(print_names_of_length())


if __name__ == "__main__":
    main()
