def is_funny(string):
    return False if False in [False if char != 'h' and char != 'a' else True for char in string] else True


def main():
    print(is_funny("hahahahahaha"))
    print(is_funny("hahajajahaha"))


if __name__ == "__main__":
    main()
