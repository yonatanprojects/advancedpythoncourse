def filter_num(num):
    if num % 4 == 0:
        return True
    else:
        return False


def four_dividers(number):
    return list(filter(filter_num, range(1, number)))


def main():
    print(four_dividers(9))
    print(four_dividers(3))


if __name__ == "__main__":
    main()
