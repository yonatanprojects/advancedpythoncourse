import functools


def add(x, y):
    return int(x) + int(y)


def sum_of_digits(number):
    return functools.reduce(add, str(number))


def main():
    print(sum_of_digits(104))


if __name__ == "__main__":
    main()
