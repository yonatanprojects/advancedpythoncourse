def find_password(password):
    return " ".join("".join([chr(ord(char) + 2) for char in password]).split("<")[1].split("\""))


def main():
    password = "sljmai ugrf rfc ambc: lglc dmsp mlc rum"
    print("The numbers for the password are" + find_password(password))


if __name__ == "__main__":
    main()
