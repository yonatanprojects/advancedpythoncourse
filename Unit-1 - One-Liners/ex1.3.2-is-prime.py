def is_prime(number):
    return False if True in [True if number % x == 0 else False for x in range(2, number-1)] else True


def main():
    print(is_prime(42))
    print(is_prime(43))


if __name__ == "__main__":
    main()
